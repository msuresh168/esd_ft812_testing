/*
 * EVE_Platform_STM32F41X.h
 *
 *  Created on: Apr 22, 2020
 *      Author: lenovo
 */

#ifndef EVE_PLATFORM_STM32F41X_H_
#define EVE_PLATFORM_STM32F41X_H_

#include "EVE_Config.h"
#include "stm32f4xx_hal.h"
#include "spi.h"
#include "gpio.h"

#define TIMEOUT_VAL 100

#define CS_ENABLE		do { HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET); } while(0);
#define CS_DISABLE		do { HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET); } while(0);


#define PD_LOW		do { HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET); } while(0);
#define PD_HIGH		do { HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET); } while(0);



#endif /* EVE_PLATFORM_STM32F41X_H_ */
